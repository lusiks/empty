using NUnit.Framework;
using Anagram;

namespace TestAnagram
{
    public class Tests
    {

        [Test]
        public void InputTest()
        {
            var inputText = " ja#pl z34n";
            var resultText = " lp#aj n34z";
            Assert.AreEqual(Anagram.Anagram.ReverseText(inputText), resultText);

        }
        [Test]
        public void NullTest()
        {
         
            Assert.IsNull(Anagram.Anagram.ReverseText(null));
        }
    }
}