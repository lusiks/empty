﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anagram
{
    public class Anagram
    {
        public static string ReverseText(string TextInput)
        {
            if (string.IsNullOrEmpty(TextInput))
            {
                return TextInput;
            }
            var intList = new List<char>();

            for (int i = 0; i < TextInput.Count(); i++)
            {
                if (char.IsWhiteSpace(TextInput[i]))
                {
                    intList.Add(TextInput[i]);
                }
                else
                {
                    int j = i;
                    for (; j < TextInput.Count(); j++)
                    {
                        if (char.IsWhiteSpace(TextInput[j]))
                        {
                            break;
                        }
                    }
                    var word = TextInput.Substring(i, j - i);
                    intList.AddRange(ReverseWord(word));
                    i = j-1;
                }
            }

            return new string(intList.ToArray());
        
        }
        private static string ReverseWord(string WordInput)
        {
            var intlist = new List<char>();
            for (int i = WordInput.Length; i > 0; i--)
            {
                if (char.IsLetter(WordInput[i - 1]))
                {
                    intlist.Add(WordInput[i - 1]);
                }
            }
            for (int i = 0; i < WordInput.Length; i++)
            {
                if (!char.IsLetter(WordInput[i]))
                {
                    intlist.Insert(i, WordInput[i]);
                }
            }
            return new string(intlist.ToArray());
        }
    }
}
