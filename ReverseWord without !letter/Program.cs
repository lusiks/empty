﻿using System;

namespace ReverseWord_without__letter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку: ");
            var Phrase = Console.ReadLine();

            if (Phrase != "")
            {
                for (int i = Phrase.Length; i > 0; i--)
                {
                    if (!char.IsLetter(Phrase[i]))
                    {
                        Console.WriteLine(Phrase[i]);
                    }
                    else
                    {
                        Console.Write(Phrase[i - 1]);
                    }
                    
                }
            }
            Console.ReadKey();
        }
    }
}
